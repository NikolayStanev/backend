import hug
from Reader import Reader
from hug.middleware import CORSMiddleware
from falcon import *
from datetime import datetime

api = hug.API(__name__)
api.http.add_middleware(CORSMiddleware(api))
reader = Reader('outages.txt')


@hug.get('/outages',
         examples='service_id=0&startTime=2020-08-19%20%2000%3A31%3A26&duration=0&endTime=2020-08-19%20%2001%3A03%3A12')
def getOutages(service_id: hug.types.number, startTime: hug.types.text,
               duration: hug.types.number, endTime: hug.types.text):
    if startTime == 0:
        raise falcon.HTTPBadRequest("message", "No startTime value!")
    try:
        return reader.find_service(service_id, startTime, duration, endTime)
    except ValueError:
        raise falcon.HTTPBadRequest("message", "Wrong date format!")


@hug.post('/create')
def createOutage(outageService: hug.types.json):
    try:
        datetime.strptime(outageService['startTime'], '%Y-%m-%d %H:%M:%S')
    except ValueError:
        raise falcon.HTTPBadRequest("message", "Wrong startDate format!")
    if outageService['service_id'] == 0 or outageService['service_id'] == "":
        raise falcon.HTTPBadRequest("message", "No service id!")
    if outageService['duration'] == 0 or outageService['duration'] == "":
        raise falcon.HTTPBadRequest("message", "No duration value!")
    if outageService['startTime'] == 0 or outageService['startTime'] == "":
        raise falcon.HTTPBadRequest("message", "No startTime value!")
    try:
        int(outageService['duration'])
    except ValueError:
        raise falcon.HTTPBadRequest("message", "Duration must be a number!")
    try:
        reader.append_service(outageService)
    except ValueError:
        raise falcon.HTTPBadRequest("message", "Wrong date format!")

# ?service_id=9999&startTime=2020-08-18%20%2001%3A00%3A42&duration=0&endTime=2020-08-19%20%2002%3A00%3A42
