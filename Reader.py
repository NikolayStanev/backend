import json
from datetime import datetime, timedelta


class Reader:
    database = 'outages.txt'

    def __init__(self, database):
        self.database = database
    """" The method can search with all given params or just with a time period.
    If no end time is given it will automatically take the current date and hour. 
    """

    def find_service(self, service_id, periodStartTime, duration, periodEndTime):
        result = {
            "resultCurrent": [],
            "resultRecent": []
        }
        if periodEndTime == 'empty':
            periodEndTime = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        try:
            period_start_time = datetime.strptime(periodStartTime, '%Y-%m-%d %H:%M:%S')
            period_end_time = datetime.strptime(periodEndTime, '%Y-%m-%d %H:%M:%S')
        except TypeError as error:
            raise error

        try:
            with open(self.database, "r") as json_file:
                outages = json.load(json_file)
                for outage in outages:

                    outage_id = outage['service_id']
                    outage_start_time = datetime.strptime(outage['startTime'], '%Y-%m-%d %H:%M:%S')
                    outage_duration = int(outage['duration'])
                    outage_end_time = outage_start_time + timedelta(minutes=outage_duration)
                    if outage_id == service_id and duration == 0 and period_start_time <= outage_start_time <= period_end_time <= outage_end_time or \
                            outage_id == service_id and duration == outage_duration and period_start_time <= outage_start_time <= period_end_time <= outage_end_time or \
                            service_id == 0 and duration == 0 and period_start_time <= outage_start_time <= period_end_time <= outage_end_time or \
                            service_id == 0 and duration == outage_duration and period_start_time <= outage_start_time <= period_end_time <= outage_end_time:
                        result["resultCurrent"].append(outage)

                    elif outage_id == service_id and duration == 0 and period_start_time <= outage_start_time <= period_end_time >= outage_end_time or \
                            outage_id == service_id and duration == outage_duration and period_start_time <= outage_start_time <= period_end_time >= outage_end_time or \
                            service_id == 0 and duration == 0 and period_start_time <= outage_start_time <= period_end_time >= outage_end_time or \
                            service_id == 0 and duration == outage_duration and period_start_time <= outage_start_time <= period_end_time >= outage_end_time:
                        result["resultRecent"].append(outage)

            return result
        except FileNotFoundError as error:
            raise error

    def append_service(self, service):
        try:
            with open(self.database, "r+") as json_file:
                data = json.load(json_file)
                data.append(service)
                json_file.seek(0)
                json_file.write(json.dumps(data))

        except FileNotFoundError as error:
            raise error

    def sortTime(self, e):
        return e['startTime']

    def isFlapping(self, e):
        if e['isFlapping'] == 'Yes':
            return True
        else:
            return False

    """"How it works
        Basically the method is using find_service again to find all 
        the outages during the given period of time. The result
        is then taken and sorted ascending by date. Then it iterates through
        the sorted array adding every new outage to the flapping @dict. Flappings(dict)
        maps the outages by their service id. Also method is adding isFlapping flag (Yes/No).
        Finally the flappings dict is filter using the isFlapping flag and the result is returned
        """

    def find_flappings(self, service_id, periodStartTime, duration, periodEndTime):
        result = self.find_service(service_id, periodStartTime, duration, periodEndTime)
        flappings = {}

        joinResult = result['resultRecent'] + result['resultCurrent']

        joinResult.sort(key=self.sortTime)
        # ---------------------------------------- Starting iteration ------------------------------------------------------
        """ 2 cycles the first removes the element from the list so that the second iteration starts from the next item.
            Since the @joinResult is sorted by time, method breaks after exceeding the @period of 2h.
            @sumOfOutages - counts every outage that is included in a flapping scenario.
            @amountOfOutages - counts how many flapping scenarios occurred during the given time period
            
            However it shows the @startingTime only of the last flapping scenario.  
        """
        for outage in joinResult[:]:

            joinResult.remove(outage)
            outageId = outage['service_id']
            period = datetime.strptime(outage['startTime'], '%Y-%m-%d %H:%M:%S') + timedelta(minutes=120)
            outage_duration = int(outage['duration'])

            tempDuration = outage_duration
            tempSumOfOutages = 1

            if outageId not in flappings or \
                    outageId in flappings and \
                    datetime.strptime(outage['startTime'], '%Y-%m-%d %H:%M:%S') > \
                    datetime.strptime(flappings[outageId]['startTime'], '%Y-%m-%d %H:%M:%S') + timedelta(minutes=120):

                for nextOutage in joinResult:
                    nextOutageStartTime = datetime.strptime(nextOutage['startTime'], '%Y-%m-%d %H:%M:%S')
                    if period < nextOutageStartTime:
                        break

                    if outageId == nextOutage['service_id']:
                        tempDuration += int(nextOutage['duration'])
                        tempSumOfOutages += 1

                if tempDuration >= 15:
                    outage['duration'] = tempDuration
                    outage['sumOfOutages'] = tempSumOfOutages
                    outage['amountOfOutages'] = 1
                    if outage['service_id'] not in flappings:
                        flappings[outageId] = outage
                    else:
                        flappings[outageId]['amountOfOutages'] += 1
                        flappings[outageId]['sumOfOutages'] += tempSumOfOutages
                        flappings[outageId]['duration'] += tempDuration
                        flappings[outageId]['startTime'] = outage['startTime']

        # -----------------------------------------------------end of iteration --------------------------------------------

        return flappings

# filtered = {k: v for k, v in flappings.items() if v['isFlapping'] == 'Yes'}

# print(find_flappings(0, '2020-07-02 09:01:31', 0, '2020-07-02  12:20:24'))

# for outage in result['resultCurrent']:
#     outageId = outage['service_id']
#     if (outage['duration']) > 15:
#         if outageId in flappings:
#             flappings[outageId]['sumOfOutages'] += 1
#             flappings[outageId]['amountOfOutages'] += outage['duration']
#         else:
#             outage['sumOfOutages'] = 1
#             outage['amountOfOutages'] = outage['duration']
#             outage['isFlapping'] = 'Yes'
#             flappings[outageId] = outage


# ----------------------------------------------------------------------------------------------------------------------

# if (outage_duration) > 15:
#     if outageId in flappings:
#         flappings[outageId]['sumOfOutages'] += 1
#         flappings[outageId]['duration'] += outage_duration
#     else:
#         outage['sumOfOutages'] = 1
#         outage['isFlapping'] = 'Yes'
#         outage['endOfPeriod'] = outage_start_time + timedelta(minutes=120)
#         flappings[outageId] = outage
#
# elif outageId in flappings:
#     flappingStartTime = datetime.strptime(flappings[outageId]['startTime'], '%Y-%m-%d %H:%M:%S')
#
#     # if flappingStartTime + 2h is bigger then the current outage start time then
#     # we add the outage duration and also add +1 to the sum of outages
#
#     if flappingStartTime + timedelta(minutes=120) >= outage_start_time:
#
#         flappings[outageId]['sumOfOutages'] += 1
#         flappings[outageId]['duration'] += outage_duration
#         if flappings[outageId]['duration'] >= 15:
#             flappings[outageId]['isFlapping'] = 'Yes'
#
#     # if flappingStartTime + 2h is less then current outage start time then 2h have passed
#     # and we reset the start time of this service in our flappings dict. We can do this
#     # because our joinResult list is sorted by time.
#
#     else:
#         flappings[outageId]['startTime'] = outage_start_time.strftime('%Y-%m-%d %H:%M:%S')
#         flappings[outageId]['sumOfOutages'] = 1
#
# # Finally if there is no such outage in the flapping dict we just add it
#
# else:
#     outage['sumOfOutages'] = 1
#     outage['duration'] = outage_duration
#     outage['isFlapping'] = 'No'
#     flappings[outageId] = outage
