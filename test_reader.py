import unittest
from Reader import Reader
reader = Reader('outagesTests.txt')

class TestReader(unittest.TestCase):


    # Test a normal scenarion in a small period of time - 1h.
    def testFlapping1(self):
        toBeTested = reader.find_flappings(0, "2020-07-02 09:01:31", 0, "2020-07-02  10:20:24").get(1704)

        self.assertEqual(toBeTested["service_id"], 1704, "Should be:  1704")
        self.assertEqual(toBeTested["duration"], 15, "Should be:  15")
        self.assertEqual(toBeTested["startTime"], "2020-07-02  09:01:31", "Should be:  2020-07-02  09:01:31")
        self.assertEqual(toBeTested["sumOfOutages"], 2, "Should be:  2")
        self.assertEqual(toBeTested["amountOfOutages"], 1, "Should be:  1")
    # Test a scenario of 5+h with 1 15 min outage in the middle.
    def testFlapping2(self):
        toBeTested = reader.find_flappings(0, "2020-07-03 :01:31", 0, "2020-07-03  22:40:24").get(1704)
        self.assertEqual(toBeTested["service_id"], 1704, "Should be:  1704")
        self.assertEqual(toBeTested["duration"], 15, "Should be:  15")
        self.assertEqual(toBeTested["startTime"], "2020-07-03  18:18:31", "Should be:  2020-07-03  18:18:31")
        self.assertEqual(toBeTested["sumOfOutages"], 2, "Should be:  2")
        self.assertEqual(toBeTested["amountOfOutages"], 1, "Should be:  1")
    # Test a scenario with a big gabs between the flappings and more then 24h given time day.
    def testFlapping3(self):
        toBeTested = reader.find_flappings(0, "2020-07-04 17:01:31", 0, "2020-07-05  23:50:24").get(1704)
        self.assertEqual(toBeTested["service_id"], 1704, "Should be:  1704")
        self.assertEqual(toBeTested["duration"], 45, "Should be:  45")
        self.assertEqual(toBeTested["startTime"], "2020-07-05  23:18:31", "Should be:  2020-07-05  23:18:31")
        self.assertEqual(toBeTested["sumOfOutages"], 5, "Should be:  5")
        self.assertEqual(toBeTested["amountOfOutages"], 3, "Should be:  3")